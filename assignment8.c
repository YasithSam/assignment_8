/** Considered subject name can have spaces**/
#include <stdio.h>

struct student
{
  char fname[30];
  char subject[30];
  double marks;
};
int main()
{
  int i,total=0;
  while(total < 5){
	  printf("Enter the number of student records(min:5): ");
		scanf("%d",&total);

		if(total < 5){
			printf("Number of students should be more than 5.\nPlease enter again.");
		}
	}
  struct student stud[total];
   for(i=0; i<total; i++)
   {
       printf("Student %d\n",i+1);
       printf("Enter student first name :\n");
       getchar();
       scanf("%[^\n]%*c",stud[i].fname);
       printf("Enter subject :\n");
       scanf("%[^\n]%*c",stud[i].subject);
       printf("Enter marks :\n");
       scanf("%lf", &stud[i].marks);
    }
    printf("----------Display stored data of students--------- \n");
    for(i=0; i<total; i++)
    {
      printf("Student %d\n",i+1);
      printf("Student Name : %s\n", stud[i].fname);
      printf("Subject : %s\n", stud[i].subject);
      printf("Marks : %.2lf\n", stud[i].marks);
    }
      
  
 
  return 0;
}

